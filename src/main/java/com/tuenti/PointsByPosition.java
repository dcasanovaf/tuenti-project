package com.tuenti;

import com.tuenti.core.play.Play;
import com.tuenti.core.position.Position;

import java.util.Objects;

public class PointsByPosition {
    private Position position;
    private Play play;
    private int points;

    public PointsByPosition(Position position, Play play){
        this.position = position;
        this.play = play;
    }

    public PointsByPosition(Position position, Play play, int points){
        this.position = position;
        this.play = play;
        this.points = points;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Play getPlay() {
        return play;
    }

    public void setPlay(Play play) {
        this.play = play;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PointsByPosition that = (PointsByPosition) o;
        return Objects.equals(position, that.position) &&
                Objects.equals(play, that.play);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, play);
    }
}
