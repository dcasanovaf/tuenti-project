package com.tuenti;

import com.tuenti.core.play.Play;
import com.tuenti.core.position.Position;

import java.util.Map;

public class Stat {
    private Player player;
    private Team team;
    private Position position;
    private Map<Play, Integer> plays;

    public Stat(Player player, Team team, Position position, Map<Play, Integer> plays){
        this.player = player;
        this.position = position;
        this.plays = plays;
        this.team = team;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Map<Play, Integer> getPlays() {
        return plays;
    }

    public void setPlays(Map<Play, Integer> plays) {
        this.plays = plays;
    }

}
