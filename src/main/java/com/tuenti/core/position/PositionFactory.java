package com.tuenti.core.position;

import com.tuenti.core.play.Assist;
import com.tuenti.core.play.Play;
import com.tuenti.core.play.Rebound;
import com.tuenti.core.play.ScoredPoint;

public class PositionFactory {
    public Position getPosition(String type){
        if(type == null){
            return null;
        }else if (type.equals("center")){
            return new Center();
        }else if (type.equals("forward")){
            return new Forward();
        }else if (type.equals("guard")){
            return new Guard();
        }

        return null;
    }
}
