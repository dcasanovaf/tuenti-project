package com.tuenti.core.position;

import java.io.Serializable;
import java.util.Objects;

public class Forward implements Position, Serializable{
    @Override
    public String getName() {
        return "forward";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(getName(), position.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
