package com.tuenti.core.play;

import com.tuenti.core.play.Assist;
import com.tuenti.core.play.Play;
import com.tuenti.core.play.Rebound;
import com.tuenti.core.play.ScoredPoint;

public class PlayFactory {
    public Play getPlay(String type){
        if(type == null){
            return null;
        }else if (type.equals("assist")){
            return new Assist();
        }else if (type.equals("rebound")){
            return new Rebound();
        }else if (type.equals("scoredPoint")){
            return new ScoredPoint();
        }

        return null;
    }
}
