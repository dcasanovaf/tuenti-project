package com.tuenti.core.play;

import java.io.Serializable;
import java.util.Objects;

public class ScoredPoint implements Play, Serializable{

    @Override
    public String getType() {
        return "scoredPoint";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Play play = (Play) o;
        return Objects.equals(getType(), play.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
