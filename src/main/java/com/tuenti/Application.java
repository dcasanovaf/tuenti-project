package com.tuenti;

import com.tuenti.*;
import com.tuenti.core.play.PlayFactory;
import com.tuenti.core.play.Play;
import com.tuenti.core.position.Position;
import com.tuenti.core.position.PositionFactory;
import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

public class Application {
    private static List<Team> teams = new ArrayList<Team>();
    private static List<Player> players = new ArrayList<Player>();
    private static List<Stat> stats = new ArrayList<Stat>();
    private static List<PointsByPosition> pointsByPositions = new ArrayList<PointsByPosition>();

    private static PlayFactory playFactory = new PlayFactory();
    private static PositionFactory positionFactory = new PositionFactory();

    public static void main(String[] args) {
        init();

        for(Stat stat : stats){
            int points = 0;

            for (Map.Entry<Play, Integer> entry : stat.getPlays().entrySet()) {

                int index = ArrayUtils.indexOf(pointsByPositions.toArray(), new PointsByPosition(stat.getPosition(), entry.getKey()));
                System.out.print("Points for " + stat.getPosition().getName() + " and " + entry.getKey().getType() + " is " + pointsByPositions.get(index).getPoints() * entry.getValue() + "\n");

                points += pointsByPositions.get(index).getPoints() * entry.getValue();

            }

            System.out.print("Total Points for " + stat.getPlayer().getName() + " , team: " + stat.getTeam().getName() + " is " + points + "\n\n");

        }
    }

    public static void init(){
        populateTeams();
        populatePlayer();
        populateStats();
        populatePointsByPosition();
    }

    private static void populateTeams(){
        teams.add(new Team("Team A"));
        teams.add(new Team("Team B"));
        teams.add(new Team("Team C"));
    }

    private static void populatePlayer(){
        players.add(new Player("Daniel","danielnick", 1));
        players.add(new Player("Juan","juannick", 2));
        players.add(new Player("David","davidlnick", 3));
    }

    private static void populateStats(){
        for(Player player : players){

            Map<Play, Integer> plays = new HashMap<Play, Integer>();
            plays.put(playFactory.getPlay("assist"), random(0, 10));
            plays.put(playFactory.getPlay("rebound"), random(0, 10));
            plays.put(playFactory.getPlay("scoredPoint"), random(0, 10));

            Stat stat = new Stat(player, teams.get(random(0, 2)), positionFactory.getPosition("guard"), plays);
            stats.add(stat);
        }
    }

    private static void populatePointsByPosition(){
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("guard"), playFactory.getPlay("scoredPoint"), 2));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("guard"), playFactory.getPlay("rebound"), 3));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("guard"), playFactory.getPlay("assist"), 1));

        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("forward"), playFactory.getPlay("scoredPoint"), 2));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("forward"), playFactory.getPlay("rebound"), 2));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("forward"), playFactory.getPlay("assist"), 2));

        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("center"), playFactory.getPlay("scoredPoint"), 2));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("center"), playFactory.getPlay("rebound"), 1));
        pointsByPositions.add(new PointsByPosition(positionFactory.getPosition("center"), playFactory.getPlay("assist"), 3));
    }

    public static int random(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
